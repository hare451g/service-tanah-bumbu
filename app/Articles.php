<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $type_id
 * @property string $title
 * @property string $content
 * @property string $description
 * @property string $division
 * @property int $read_count
 * @property string $created_at
 * @property string $updated_at
 * @property Type $type
 * @property User $user
 */
class Articles extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'type_id', 'title', 'content', 'description', 'division', 'read_count', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
