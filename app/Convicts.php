<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $ktp_number
 * @property string $name
 * @property string $address
 * @property string $city
 * @property string $province
 * @property string $post_code
 * @property string $email
 * @property string $gender
 * @property string $created_at
 * @property string $updated_at
 * @property Good[] $goods
 * @property Ticket[] $tickets
 */
class Convicts extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['ktp_number', 'name', 'address', 'city', 'province', 'post_code', 'email', 'gender', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function goods()
    {
        return $this->hasMany('App\Good');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }
}
