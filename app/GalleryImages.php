<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $gallery_id
 * @property int $images_id
 * @property int $user_id
 * @property string $created_at
 * @property string $updated_at
 * @property Gallery $gallery
 * @property Image $image
 */
class GalleryImages extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['gallery_id', 'images_id', 'user_id', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo('App\Image', 'images_id');
    }
}
