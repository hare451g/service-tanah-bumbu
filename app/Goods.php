<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $profile_id
 * @property int $convict_id
 * @property int $type_id
 * @property string $charges
 * @property string $informations
 * @property string $created_at
 * @property string $updated_at
 * @property Convict $convict
 * @property Profile $profile
 * @property Type $type
 */
class Goods extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['profile_id', 'convict_id', 'type_id', 'charges', 'informations', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function convict()
    {
        return $this->belongsTo('App\Convict');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function profile()
    {
        return $this->belongsTo('App\Profile');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Type');
    }
}
