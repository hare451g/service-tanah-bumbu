<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $type_id
 * @property string $name
 * @property string $path
 * @property string $descriptions
 * @property string $created_at
 * @property string $updated_at
 * @property Type $type
 * @property User $user
 * @property GalleryImage[] $galleryImages
 */
class Images extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'type_id', 'name', 'path', 'descriptions', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function galleryImages()
    {
        return $this->hasMany('App\GalleryImage', 'images_id');
    }
}
