<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property string $nip
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property string $division
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Good[] $goods
 */
class Profiles extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'nip', 'first_name', 'last_name', 'gender', 'division', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function goods()
    {
        return $this->hasMany('App\Good');
    }
}
