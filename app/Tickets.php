<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $convict_id
 * @property int $type_id
 * @property string $ticket_number
 * @property string $trial_date
 * @property string $stnk_number
 * @property string $name_on_sim
 * @property string $date_visit
 * @property string $gender
 * @property string $created_at
 * @property string $updated_at
 * @property Convict $convict
 * @property Type $type
 */
class Tickets extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['convict_id', 'type_id', 'ticket_number', 'trial_date', 'stnk_number', 'name_on_sim', 'date_visit', 'gender', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function convict()
    {
        return $this->belongsTo('App\Convict');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Type');
    }
}
