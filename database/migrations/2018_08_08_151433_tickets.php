<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('convict_id');
            $table->unsignedInteger('type_id');
            $table->string('ticket_number');
            $table->string('trial_date');
            $table->string('stnk_number');
            $table->string('name_on_sim')->nullable();
            $table->string('date_visit');
            $table->string('gender', 1);
            $table->timestamps();
            $table->foreign('convict_id')->references('id')->on('convicts');
            $table->foreign('type_id')->references('id')->on('types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
