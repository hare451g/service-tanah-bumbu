<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Goods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goods', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('profile_id');
            $table->unsignedInteger('convict_id');
            $table->unsignedInteger('type_id');
            $table->string('charges');
            $table->longText('informations');
            $table->timestamps();
            $table->foreign('type_id')->references('id')->on('types');
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->foreign('convict_id')->references('id')->on('convicts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('goods');
    }
}
