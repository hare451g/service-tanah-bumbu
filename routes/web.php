<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/**
 * Routes for resource articles
 */
$app->get('articles', 'ArticlesController@all');
$app->get('articles/{id}', 'ArticlesController@get');
$app->post('articles', 'ArticlesController@add');
$app->put('articles/{id}', 'ArticlesController@put');
$app->delete('articles/{id}', 'ArticlesController@remove');
